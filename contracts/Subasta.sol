pragma solidity ^0.4.0;

import "./ERC20Token.sol";
import "./SubastaRules.sol";

contract Subasta  is SubastaRules{
    

    uint256 private constant THREE_DAYS =  1000*60;
    address public owner;
    ERC20Token private _token;
    address public winner;
    address public wining;
    uint256 public highestOffer;
    
    function Subasta(ERC20Token token) public {
    
        _token = token;
        owner = msg.sender;
        startTime = now;
        endTime = startTime + THREE_DAYS;
    }
    
    function makeAnOffer( uint256 value ) checkingState atState(States.OPEN)  public {
    
        require(value > highestOffer);
        wining = msg.sender;
        highestOffer = value;
    }
    
    function winingCheck()  public view returns (bool){
       
       return  wining == msg.sender;
    }
    
    function deposit() checkingState atState(States.CLOSED) public {
    
        require(msg.sender == wining);
        require(_token.allowance(msg.sender, owner) >= highestOffer);
        
        _token.transferFrom(msg.sender, owner, highestOffer);
        winner = msg.sender;
        //retira los tokens de las cuentas del ganador y lo deposita en la cuenta del owner.
        //y deja asentado que el ganador ya pagó los tokens que oferto en la subasta.
    }
}
