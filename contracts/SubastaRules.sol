pragma solidity ^0.4.0;

contract SubastaRules {
    
    uint256 public startTime;
    uint256 public endTime;
   
    enum States {
        OPEN,
        CLOSED
    }
    
    States public state = States.OPEN;
    
    modifier atState(States _state){
        
        require(state == _state);
        _;
    }
    
    modifier checkingState(){
        
        if ( now <= endTime ){
            state = States.OPEN;
        }
        else {
            state = States.CLOSED;
        }
        _;
    }
}
