pragma solidity ^0.4.0;


library SafeMath {
    function add(uint a, uint b) internal pure returns (uint c) {
        c = a + b;
        require(c >= a);
    }
    function sub(uint a, uint b) internal pure returns (uint c) {
        require(b <= a);
        c = a - b;
    }
    function mul(uint a, uint b) internal pure returns (uint c) {
        c = a * b;
        require(a == 0 || c / a == b);
    }
    function div(uint a, uint b) internal pure returns (uint c) {
        require(b > 0);
        c = a / b;
    }
}

/*

contract ERC20Interface {
    
    
    function totalSupply() public constant returns (uint);
    function balanceOf(address tokenOwner) public constant returns (uint balance);
    function allowance(address tokenOwner, address spender) public constant returns (uint remaining);
    function transfer(address to, uint tokens) public returns (bool success);
    function approve(address spender, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);
    
    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}

*/


contract ERC20Token {
    
    uint private _totalSupply;
    mapping(address => uint) private _balances;
    mapping(address => mapping(address => uint256)) private _allowed;
    
    string public constant name = "Token_0_0_1";
    string public constant symbol = "TAR";
    uint8 public constant decimals = 18;
    
    function transfer(address to, uint256 value) public returns (bool success) {
    
        require(to != address(0));
        require(value <= _balances[msg.sender]);
        _balances[msg.sender] = SafeMath.sub(_balances[msg.sender] , value);
        _balances[to] = SafeMath.add(_balances[to], value);
        return true;
    }
    
    function transferFrom(address from, address to, uint tokens) public returns (bool success){
    
        require(to != address(0));
        require(tokens <= _balances[from]);
        require(allowance(from, msg.sender) >= tokens);
        _balances[from] = SafeMath.sub(_balances[from] , tokens);
        _balances[to] = SafeMath.add(_balances[to], tokens);
        _allowed[from][msg.sender] = SafeMath.sub(_allowed[from][msg.sender] , tokens);
        return true;
    }
    
    function totalSupply() public view returns ( uint256 ) {
        
        return _totalSupply;
    }
    
    function balanceOf(address owner) public view returns ( uint256 ){

        return _balances[owner];
    }
    
    
    function() payable  external{
        
        _balances[msg.sender] = SafeMath.add( _balances[msg.sender], msg.value);
        _totalSupply = SafeMath.add(_totalSupply,msg.value);
    }
    
    function approve(address spender, uint tokens)  public returns (bool success){
        
        _allowed[msg.sender][spender] = tokens;
        return true;
    
    }
    
    function allowance(address owner, address spender) public view returns (uint256) {
        return _allowed[owner][spender];
    }
}