const ERC20Token = artifacts.require("ERC20Token")
var simple = require('simple-mock');

const ONE_ETHER =  1000000000000000000;


contract('ERC20Token', accounts => {
    
    beforeEach(async function () {
    
    });
    
    describe("Method totalSupply() :", async function(){
     
        it('returns the sum of all account balance', async function(){
            
            //context
            let instance = await ERC20Token.deployed();
            let accountOne = accounts[1];
            let accountTwo = accounts[2];
            
            await web3.eth.sendTransaction({from:accountOne,to: ERC20Token.address, value: ONE_ETHER });
            await web3.eth.sendTransaction({from:accountTwo,to: ERC20Token.address, value: ONE_ETHER });
           
            //expected
            let expectedTotalBalance = ONE_ETHER + ONE_ETHER;
            let actualTotalBalance =  await instance.totalSupply();
            
            //l
            assert.equal( actualTotalBalance.toNumber() , expectedTotalBalance);
        })
    })
     
     
     describe("Method Transfer(): ", async function( ){
        
        describe("if sender has not sufficient balance: ", async function(){
                  
                    it("-has to revert the whole transaction - without gas consumption.", async function(){
                     
                        //context
                        let instance = await ERC20Token.deployed();
                        let sender = accounts[0];
                        let receiver = accounts[1];
                        let value = web3.toWei(1, "ether");
                        
                        try{
                            await instance.transfer(receiver, value, {from : sender});
                            assert.isOk(false,"sender has not sufficient balance to do the transfer.");
                        }
                        catch(error){
                             assert.isOk(true, "because sender has not sufficiente balance.");
                        }
                    })
                 
                    it("-can send value zero as transaction amount.  Remember: zero value transactions are valid.  ", async function(){
                        //context
                        let instance = await ERC20Token.deployed();
                        let sender = accounts[0];
                        let receiver = accounts[1];
                        let ES_ZERO = 0;
                        
                        try{
                            await instance.transfer(receiver, ES_ZERO, {from : sender});
                            assert.isOk(true,"transactions with value zero are valid.");
                        }
                        catch(error){
                             assert.isOk(true, "this zero value transaction may have been value.");
                        }
                    })
                  
                   it("-is prohibited  transfers to address zero.", async function(){
                         
                         //context
                         let instance = await ERC20Token.deployed();
                         let ZERO_ADDRESS = 0;
                         let receiver = accounts[1];
                         let sender = accounts[0];
           
                         // exercise
                        try {
                            await instance.transfer( ZERO_ADDRESS, ZERO_ADDRESS, {from: sender} );
                            assert.isOk(false, "transfer to zero addres are prohibited.");
                         
                        }
                        catch(error){
                            assert.isOk(true, "transactions to zero address prohibited.");
                        }
                   })
                  
        })
         
        
        describe("If sender has sufficient balance: ", async function(){
        
             it("-can transfer tokens to receiver.", async function(){
            
                 //context
                 let instance = await ERC20Token.deployed();
                 let sender = accounts[1];
                 let receiver = accounts[2];
                 let quantityToTransfer = ONE_ETHER;
                 await web3.eth.sendTransaction({from:sender, to: ERC20Token.address, value: ONE_ETHER });

                 //exercise
                 let isOk = await instance.transfer.call(receiver , quantityToTransfer, {from: sender});

                //test
                 assert.isTrue( isOk );
            })
            
            it("-has to reduce the sender balance.", async function(){
                        
                 //context
                 let instance = await ERC20Token.deployed();
                 let sender = accounts[1];
                 let receiver = accounts[2];
                 let quantityToTransfer = ONE_ETHER;
                 await web3.eth.sendTransaction({from:sender, to: ERC20Token.address, value: ONE_ETHER });
                 let initialBalance = await instance.balanceOf(sender);
                 
                 //exercise
                 await instance.transfer(receiver , quantityToTransfer, {from: sender});
                 
                 //expected
                 let finalBalance =  await instance.balanceOf(sender);
                 
                //test
                 assert.equal(finalBalance.toNumber(),  initialBalance.toNumber() - ONE_ETHER );
            })
            
            it("-has to increment the receiver balance.", async function(){
                 
                 //context
                 let instance = await ERC20Token.deployed();
                 let sender = accounts[1];
                 let receiver = accounts[2];
                 let quantityToTransfer = ONE_ETHER;
                 await web3.eth.sendTransaction({from:sender, to: ERC20Token.address, value: ONE_ETHER });
                 let initialBalance = await instance.balanceOf(receiver);
                 
                 //exercise
                 await instance.transfer(receiver , quantityToTransfer, {from: sender});
                 
                 //expected
                 let finalBalance =  await instance.balanceOf(receiver);
                 
                //test
                 assert.equal(finalBalance.toNumber(),  initialBalance.toNumber() + ONE_ETHER );
            })
        })
     })
     
     describe("Method transferFrom(): ", async function( ){
         
         describe("if sender has not sufficient balance: ", async function(){
                   
             it("-has to revert the whole transaction - without gas consumption.", async function(){
              
                 //context
                 let instance = await ERC20Token.deployed();
                 let sender = accounts[4];
                 let receiver = accounts[5];
                 let value = web3.toWei(1, "ether");
                 
                 try{
                     await instance.transferFrom(sender, receiver, value);
                     assert.isOk(false,"sender has not sufficient balance to do the transfer.");
                 }
                 catch(error){
                     
                     assert.isOk(true, "because sender has not sufficiente balance.");
                 }
             })
          
             it("-can send value zero as transaction amount.  Remember: zero value transactions are valid.  ", async function(){
                 //context
                 let instance = await ERC20Token.deployed();
                 let sender = accounts[0];
                 let receiver = accounts[1];
                 let ES_ZERO = 0;
                 
                 try{
                     await instance.transferFrom(sender, receiver, ES_ZERO);
                     assert.isOk(true,"transactions with value zero are valid.");
                 }
                 catch(error){
                      assert.isOk(true, "this zero value transaction may have been value.");
                 }
             })
           
            it("-is prohibited  transfers to address zero.", async function(){
                  
                  //context
                  let instance = await ERC20Token.deployed();
                  let ZERO_ADDRESS = 0;
                  let receiver = accounts[1];
                  let sender = accounts[0];
    
                  // exercise
                 try {
                     await instance.transferFrom(sender, ZERO_ADDRESS, ZERO_ADDRESS );
                     assert.isOk(false, "transfer to zero addres are prohibited.");
                  
                 }
                 catch(error){
                     assert.isOk(true, "transactions to zero address prohibited.");
                 }
            })
           
            it("-si no está autorizado debe devolver false, no tirar exception", async function(){
                    
                    assert.isTrue(true);
            })
          })
          
         
         describe("If sender has sufficient balance: ", async function(){
        
              it("-sender has to be allowed to do transfer from another account", async function(){
              
                //context
                let instance = await ERC20Token.deployed();
                let owner = accounts[1];
                let receiver = accounts[2];
                let spender = accounts[3];
                let quantityToTransfer = 2 * ONE_ETHER;
                await web3.eth.sendTransaction({from:owner, to: ERC20Token.address, value: ONE_ETHER });
                await instance.approve(spender,ONE_ETHER, {from: owner} );
                
                //exercise
                try{
                    await instance.transferFrom(owner,receiver,quantityToTransfer, {from: spender} );
                    assert.isOk(false, "owner is allowed to transfer only one ether from owner account.Not two.");
                } catch(error){
                    assert.isOk(true, "because owner was not allowed to transfer two ether.");
                }
              })
              
              
              it("-can transfer tokens from another account to receiver.", async function(){
             
                  //context
                  let instance = await ERC20Token.deployed();
                  let spender = accounts[1];
                  let receiver = accounts[2];
                  let owner = accounts[3];
                  let quantityToTransfer = ONE_ETHER;
                  await instance.approve(spender,ONE_ETHER, {from: owner} );
                  await web3.eth.sendTransaction({from:owner, to: ERC20Token.address, value: ONE_ETHER });
 
                  //exercise
                  let isOk = await instance.transferFrom.call(owner, receiver , quantityToTransfer, {from: spender } );
 
                 //test
                  assert.isTrue( isOk );
             })
             
             it("-has to reduce the sender balance.", async function(){
                  
                  //context
                  let instance = await ERC20Token.deployed();
                  let spender = accounts[1];
                  let receiver = accounts[2];
                  let owner = accounts[3];
                  let quantityToTransfer = ONE_ETHER;
                  await instance.approve(spender,ONE_ETHER, {from: owner} );
                  await web3.eth.sendTransaction({from:owner, to: ERC20Token.address, value: ONE_ETHER });
                  let initialBalance = await instance.balanceOf(owner);
                  
                  //exercise
                  await instance.transferFrom(owner, receiver , quantityToTransfer , {from: spender} );
                  
                  //expected
                  let finalBalance =  await instance.balanceOf(owner);
                  
                 //test
                  assert.equal(finalBalance.toNumber(),  initialBalance.toNumber() - ONE_ETHER );
             })
             
             it("-has to increment the receiver balance.", async function(){
                  
                  //context
                  let instance = await ERC20Token.deployed();
                  let spender = accounts[1];
                  let receiver = accounts[2];
                  let owner = accounts[3];
                  let quantityToTransfer = ONE_ETHER;
                  await instance.approve(spender, ONE_ETHER, {from: owner});
                  await web3.eth.sendTransaction({from:owner, to: ERC20Token.address, value: ONE_ETHER });
                  
                  //exercise
                  let initialBalance = await instance.balanceOf(receiver);
                  await instance.transferFrom(owner, receiver , quantityToTransfer, {from: spender});
                  
                  //expected
                  let finalBalance =  await instance.balanceOf(receiver);
                  
                 //test
                  assert.equal(finalBalance.toNumber(),  initialBalance.toNumber() + ONE_ETHER );
             })
             
             it("-has to reduce the amount that an spender is approved to transfer.", async function(){
                               
                   //context
                   let instance = await ERC20Token.deployed();
                   let spender = accounts[1];
                   let receiver = accounts[2];
                   let owner = accounts[3];
                   let quantityToTransfer = ONE_ETHER;
                   await web3.eth.sendTransaction({from:owner, to: ERC20Token.address, value: ONE_ETHER });
                   
                   //exercise
                   await instance.approve(spender, ONE_ETHER, {from: owner});
                   let initialApproved = await instance.allowance(owner, spender);
                   await instance.transferFrom(owner, receiver , quantityToTransfer, {from: spender});
                   
                   //expected
                   let finalApproved = await instance.allowance(owner, spender);
                   
                  //test
                   assert.equal(initialApproved.toNumber() - ONE_ETHER,  finalApproved.toNumber()  );
             })
         })
     })
     
     describe("Method balanceOf: ", async function(){
        
        it("-has to return the balance for an account.", async function(){
            //context
            let instance = await ERC20Token.deployed();
            let receiver = accounts[2];
            let quantityToTransfer = ONE_ETHER;
            let initialBalance = await instance.balanceOf(receiver);
            await web3.eth.sendTransaction({from:receiver, to: ERC20Token.address, value: ONE_ETHER });
            
            //exercise
            let finalBalance = await instance.balanceOf(receiver);
            
            //test
            assert.equal(initialBalance.toNumber() + ONE_ETHER, finalBalance);
        })
     });
     
     
     describe("Method approved(): ", async function(){
        
        it("-allows spender to spend others accounts token.", async function(){
            
            //context
            let instance = await ERC20Token.deployed();
            let owner = accounts[2];
            let spender = accounts[3];
            let otherAccount = accounts[3];
            let approvedValue = ONE_ETHER;
            
            //exercise
            await instance.approve(spender, approvedValue, {from : owner});
            
            //test
            let actualApprovedValue = await instance.allowance(owner, spender);
            
            assert.equal(actualApprovedValue.toNumber(), approvedValue);
        })
     });
     
     
    
   /* it("cuenta sobre el gas usado", async function(){
      
           let accountOne = accounts[1];
           let accountTwo = accounts[2];
           let initialBalance =  web3.eth.getBalance(accountOne);
           let oneEther = 1000000000000000000;
           
           let transactionHash  = web3.eth.sendTransaction(
                { from : accountOne, to: accountTwo, value : oneEther }
           );
           
           var data = await web3.eth.getTransaction(transactionHash);
           
           var totalGas = data.gas * data.gasPrice;
           console.log("total gas: " +  totalGas );
           console.log("balance inicial: " +  initialBalance);
           console.log("balance cuenta uno sin descontar gas: " + ( initialBalance - oneEther ));
           console.log("balance cuenta uno con gas descontado: " + ( initialBalance - oneEther -  totalGas));
           console.log("cuenta uno final:" + web3.eth.getBalance(accountOne));
           console.log("cuenta dos:" + web3.eth.getBalance(accountTwo));
           
           assert.isTrue(false, "test falla porque no dan las cuentas");
        
       });*/
})