const Subasta = artifacts.require("Subasta")
const ERC20Token = artifacts.require("ERC20Token")

var simple = require('simple-mock');

const ONE_ETHER =  1000000000000000000;


contract('Subasta.js', accounts => {


    describe("Subasta: ", async function(){
    
        it("-has to have a start time", async function(){

            //context
            let token = await ERC20Token.deployed();
            let instance = await Subasta.deployed(token);
            
            //exercise
            let actualStartTime = await instance.startTime();
            
            //test
            assert.isNotNull(actualStartTime);
        })
        
        it("-has to have a fixed duration time of 3 days", async function(){

            //context
            let token = await ERC20Token.deployed();
            let instance = await Subasta.deployed(token);
            let ONE_DAY = 1000*60*60*24;
            let THREE_DAYS = 3 * ONE_DAY;
            
            //expected
            let expectedEndTime = await instance.startTime();
            expectedEndTime = expectedEndTime.toNumber() + 1000*60;
            
            //exercise
            let actualEndTime = await instance.endTime();
            
            //test
            assert.equal(actualEndTime.toNumber(), expectedEndTime);
        })
        
        it("-Offer have to be done before endTime:", async function(){
            
            assert.isTrue(true);
        });
   
        describe("Method makeAnOffer(): ", async function(){
           
           describe("If not any offer was done: ", async function(){
           
               /*
                //SOLO ANDA SI DECLARO VIEW AL METODO, PERO SI HAGO ESO NO ME ANDA
                //EL TEST DE ABAJO.
                it("the first offer is always accepted:", async function(){
                
                    //context
                    let instance = await Subasta.new( );
                    let sender = accounts[0];
                    
                    //exercise
                    let isOk = await instance.makeAnOffer(10000, {from : sender});
                    
                    //test
                     assert.isTrue(isOk);
                });
                */
                it("-the first offer is always the highest:", async function(){
                    
                    //context
                    let token = await ERC20Token.deployed();
                    let instance = await Subasta.deployed(token);
                    let sender = accounts[0];
                    
                    //exercise
                    await instance.makeAnOffer(10000, {from : sender});
                    let higherOffer = await instance.highestOffer();
                    
                    //test
                     assert.equal(higherOffer.toNumber(), 10000);
                });
                
                it("-the wining  is always the first offerer:", async function(){
                    
                    //context
                    let token = await ERC20Token.deployed();
                    let instance = await Subasta.new(token.address);
                    let sender = accounts[0];
                    
                    //exercise
                    await instance.makeAnOffer(1000, {from : sender});
                    let wining = await instance.wining();
                    
                    //test
                     assert.equal(sender,wining);
                });
           });
           
           describe("When previous offer was done: ", async function(){
           
                let instance;
                
                beforeEach(async function () {
                    
                    let token = await ERC20Token.deployed();
                    instance = await Subasta.new(token.address);
                    await instance.makeAnOffer(1000, {from : accounts[9]});
                    
                });
                
                
                it("-lower offers are not accepted", async function(){
                
                    let loserOfferer = accounts[3];
                    try{
                        
                        await instance.makeAnOffer(100, {from : loserOfferer});
                        assert.isOk(false, "This transactions is not possible because of it is lower.")
                    }catch(error){
                        
                        assert.isOk(true, "Because is not the highest offer.")
                    }
                    
                });
                
                it("-only a higher offer is registered", async function(){
                
                    let higgerOfferer = accounts[3];
                    try{
                        
                        await instance.makeAnOffer(10000, {from : higgerOfferer});
                        let newHighestOffer =  await instance.highestOffer();
                        assert.equal(newHighestOffer,10000 , "This offer is new higher.")
                   
                    }catch(error){
                        assert.isOk(false, "Because is an new higger offer.")
                    }
                });
                
                it("-the new wining is who did the new highest offer", async function(){
                    
                    let newHighestOfferer = accounts[3];
                    try{
                        
                        await instance.makeAnOffer(100000, {from : newHighestOfferer});
                        let wining =  await instance.wining();
                        assert.equal(newHighestOfferer, wining );
                   
                    }catch(error){
                        assert.isOk(false, "Because should have had a new winner.")
                    }
                });
                
           });
        });
        
        describe("Method winingCheck(): ", async function(){
        
            it("-Has a method to check if I am wining", async function(){
                let token = await ERC20Token.deployed();
                let instance = await Subasta.new(token.address);
                let lowerOfferer = accounts[3];
                let middleOfferer = accounts[4];
                let higherOfferer = accounts[5];
                
                try{
                    await instance.makeAnOffer(1000, {from : lowerOfferer });
                    await instance.makeAnOffer(5000, {from : middleOfferer});
                    await instance.makeAnOffer(10000, {from : higherOfferer});
                    
    
                    let isWining = await instance.winingCheck({from: middleOfferer});
                    assert.isFalse(isWining);
                    
                }catch(error){
                    console.log(error);
                    assert.isOk(false, "Because is an new higger offer.")
                }
            });
        });
        
        describe("Method deposit(): ", async function(){
            
            it("-cant be invoked only for the winner.", async function(){
                let token = await ERC20Token.deployed();
                let instance = await Subasta.new(token.address);
                let lowerOfferer = accounts[3];
                let higherOfferer = accounts[5];
                await instance.makeAnOffer(1000, {from : lowerOfferer });
                await instance.makeAnOffer(10000, {from : higherOfferer});
                try{
                
                    await instance.deposit({from: lowerOfferer});
                    assert.isOk(false, "Only the winner can deposit token to contract owner address.");

                }catch(error){
                    assert.isOk(true, "Only the winner can deposit token to contract owner address.");
                }
            });
            
            it("-wining address has to approve the highest value in tokens in order to be the winner.", async function(){
                let token = await ERC20Token.deployed();
                let higherOfferer = accounts[5];
                await web3.eth.sendTransaction({from:higherOfferer,to: token.address, value: ONE_ETHER });

                let instance = await Subasta.new(token.address);
                let lowerOfferer = accounts[3];
               
                let initialBalance = await token.balanceOf(higherOfferer);
                await instance.makeAnOffer(1000, {from : lowerOfferer });
                await instance.makeAnOffer(10000, {from : higherOfferer});
                let subastaOwner = await instance.owner();
                await token.approve(subastaOwner, 10000, {from: higherOfferer});
                
                try{
                    await instance.deposit({from: higherOfferer});
                    let finalBalance = await token.balanceOf(higherOfferer);
                    assert.equal(1,1 );

                }catch(error){
                    assert.isOk(false, "ESTE TEST NO ANDUVO.");
                }
            });
            
        });
    });
})