var token = artifacts.require("./ERC20Token.sol");
var subasta = artifacts.require("./Subasta.sol");


module.exports = function(deployer) {
   deployer.deploy(token)
   .then(function() {
     return deployer.deploy(subasta, token.address);
   });
};

